//Tianrui Xu 2238329
package application;
import vehicles.*;
public class BikeStore {
    
    public static void main(String[] args) {
        Bicycle[] bicycles = new Bicycle[4];
        bicycles[0] = new Bicycle("AIST", 24, 40);
        bicycles[1] = new Bicycle("Dawes", 44, 35);
        bicycles[2] = new Bicycle("Felt", 35, 50);
        bicycles[3] = new Bicycle("K2", 42, 28);

        for(Bicycle i : bicycles) {
            System.out.println(i);
        }
    }

}
